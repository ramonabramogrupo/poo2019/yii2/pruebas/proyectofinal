﻿-- 
-- Disable foreign keys
-- 
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;

-- 
-- Set SQL mode
-- 
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- 
-- Set character set the client will use to send SQL statements to the server
--
SET NAMES 'utf8';

DROP DATABASE IF EXISTS proyectoFinal;

CREATE DATABASE IF NOT EXISTS proyectoFinal
CHARACTER SET utf8
COLLATE utf8_spanish_ci;

--
-- Set default database
--
USE proyectoFinal;

--
-- Create table `tipostest`
--
CREATE TABLE IF NOT EXISTS tipostest (
  tipo varchar(50) NOT NULL,
  numero int(11) DEFAULT 0,
  PRIMARY KEY (tipo)
)
ENGINE = INNODB;


--
-- Create table `test`
--
CREATE TABLE IF NOT EXISTS test (
  id int(11) NOT NULL AUTO_INCREMENT,
  descripcion varchar(1000) DEFAULT NULL,
  fecha timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP(),
  tipo varchar(50) DEFAULT NULL,
  titulo varchar(60) DEFAULT NULL,
  nombre varchar(255) DEFAULT NULL COMMENT 'este es el campo que se utilizara realmente al imprimir el test',
  PRIMARY KEY (id)
)
ENGINE = INNODB;


DELIMITER $$

--
-- Create trigger `trigger3`
--
CREATE 
	DEFINER = 'root'@'localhost'
TRIGGER IF NOT EXISTS trigger3
	BEFORE INSERT
	ON test
	FOR EACH ROW
BEGIN
  DECLARE n int;

UPDATE tipostest t
SET t.numero = numero + 1
WHERE t.tipo = new.tipo;

SELECT
  numero INTO n
FROM tipostest t
WHERE t.tipo = new.tipo;

SET new.titulo = CONCAT('Test de ', new.tipo, ' numero ', n);
  IF new.nombre is null THEN
SET new.nombre = new.titulo;
  END IF;
END
$$

--
-- Create trigger `trigger4`
--
CREATE 
	DEFINER = 'root'@'localhost'
TRIGGER IF NOT EXISTS trigger4
	BEFORE UPDATE
	ON test
	FOR EACH ROW
BEGIN
  DECLARE n int;
  
  IF new.tipo<>old.tipo THEN
UPDATE tipostest t
SET t.numero = numero + 1
WHERE t.tipo = new.tipo;
UPDATE tipostest t
SET t.numero = numero + 1
WHERE t.tipo = old.tipo;
SELECT
  numero INTO n
FROM tipostest t
WHERE t.tipo = new.tipo;
SET new.titulo = CONCAT('Test de ', new.tipo, ' numero ', n);
  END IF;

  IF new.nombre='' THEN
SET new.nombre = new.titulo;
  END IF;
 
END
$$

DELIMITER ;

--
-- Create foreign key
--
ALTER TABLE test
ADD CONSTRAINT fktesttipos FOREIGN KEY (tipo)
REFERENCES tipostest (tipo) ON UPDATE CASCADE;

--
-- Create table `imagenes`
--
CREATE TABLE IF NOT EXISTS imagenes (
  id int(11) NOT NULL AUTO_INCREMENT,
  url varchar(200) DEFAULT NULL,
  titulo varchar(200) DEFAULT NULL,
  PRIMARY KEY (id)
)
ENGINE = INNODB;


--
-- Create table `respuestas`
--
CREATE TABLE IF NOT EXISTS respuestas (
  id int(11) NOT NULL AUTO_INCREMENT,
  enunciado varchar(200) DEFAULT NULL,
  imagen int(11) DEFAULT NULL,
  PRIMARY KEY (id)
)
ENGINE = INNODB,
AUTO_INCREMENT = 1628,
AVG_ROW_LENGTH = 98,
CHARACTER SET latin1,
COLLATE latin1_swedish_ci;

--
-- Create foreign key
--
ALTER TABLE respuestas
ADD CONSTRAINT fkrespuestaimagenes FOREIGN KEY (imagen)
REFERENCES imagenes (id) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Create table `pregunta`
--
CREATE TABLE IF NOT EXISTS pregunta (
  id int(11) NOT NULL AUTO_INCREMENT,
  enunciado varchar(200) DEFAULT NULL,
  imagen int(11) DEFAULT NULL,
  correcta char(1) DEFAULT NULL,
  PRIMARY KEY (id)
)
ENGINE = INNODB;

--
-- Create foreign key
--
ALTER TABLE pregunta
ADD CONSTRAINT fkpreguntaimagenes FOREIGN KEY (imagen)
REFERENCES imagenes (id) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Create table `preguntastest`
--
CREATE TABLE IF NOT EXISTS preguntastest (
  id int(11) NOT NULL AUTO_INCREMENT,
  pregunta int(11) DEFAULT NULL,
  test int(11) DEFAULT NULL,
  PRIMARY KEY (id)
)
ENGINE = INNODB;

--
-- Create index `pregunta` on table `preguntastest`
--
ALTER TABLE preguntastest
ADD UNIQUE INDEX pregunta (pregunta, test);

--
-- Create foreign key
--
ALTER TABLE preguntastest
ADD CONSTRAINT fkpreguntas FOREIGN KEY (pregunta)
REFERENCES pregunta (id) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Create foreign key
--
ALTER TABLE preguntastest
ADD CONSTRAINT fkpreguntastest FOREIGN KEY (test)
REFERENCES test (id) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Create table `pertenecen`
--
CREATE TABLE IF NOT EXISTS pertenecen (
  id int(11) NOT NULL AUTO_INCREMENT,
  pregunta int(11) DEFAULT NULL,
  respuesta int(11) DEFAULT NULL,
  correcta tinyint(1) DEFAULT 0,
  PRIMARY KEY (id)
)
ENGINE = INNODB;

--
-- Create index `pregunta` on table `pertenecen`
--
ALTER TABLE pertenecen
ADD UNIQUE INDEX pregunta (pregunta, respuesta);

DELIMITER $$

--
-- Create trigger `trigger1`
--
CREATE 
	DEFINER = 'root'@'localhost'
TRIGGER IF NOT EXISTS trigger1
	AFTER INSERT
	ON pertenecen
	FOR EACH ROW
BEGIN
DECLARE numero int DEFAULT 0;
DECLARE letra char(1);
IF(new.correcta) THEN
SELECT
  COUNT(*) INTO numero
FROM pertenecen p
WHERE p.pregunta = new.pregunta;
  CASE numero
    WHEN 1 THEN
SET letra = 'a';
    WHEN 2 THEN
SET letra = 'b';
    WHEN 3 THEN
SET letra = 'c';
    WHEN 4 THEN
SET letra = 'd';
  END CASE;
UPDATE pregunta p
SET correcta = letra
WHERE p.id = new.pregunta;
END IF;

END
$$

--
-- Create trigger `trigger2`
--
CREATE 
	DEFINER = 'root'@'localhost'
TRIGGER IF NOT EXISTS trigger2
	AFTER UPDATE
	ON pertenecen
	FOR EACH ROW
BEGIN
DECLARE numero int DEFAULT 0;
DECLARE letra char(1);
IF(new.correcta) THEN
SELECT
  COUNT(*) INTO numero
FROM pertenecen p
WHERE p.pregunta = new.pregunta
AND p.id <= new.id;
  CASE numero
    WHEN 1 THEN
SET letra = 'a';
    WHEN 2 THEN
SET letra = 'b';
    WHEN 3 THEN
SET letra = 'c';
    WHEN 4 THEN
SET letra = 'd';
  END CASE;
UPDATE pregunta p
SET correcta = letra
WHERE p.id = new.pregunta;
END IF;

END
$$

DELIMITER ;

--
-- Create foreign key
--
ALTER TABLE pertenecen
ADD CONSTRAINT fkpertenecenpreguntas FOREIGN KEY (pregunta)
REFERENCES pregunta (id) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Create foreign key
--
ALTER TABLE pertenecen
ADD CONSTRAINT fkpertenecenrespuestas FOREIGN KEY (respuesta)
REFERENCES respuestas (id) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Create table `categorias`
--
CREATE TABLE IF NOT EXISTS categorias (
  id int(11) NOT NULL AUTO_INCREMENT,
  texto varchar(200) DEFAULT NULL,
  descripcion varchar(200) DEFAULT NULL,
  PRIMARY KEY (id)
)
ENGINE = INNODB;


--
-- Create table `categoriastest`
--
CREATE TABLE IF NOT EXISTS categoriastest (
  id int(11) NOT NULL AUTO_INCREMENT,
  test int(11) DEFAULT NULL,
  categoria int(11) DEFAULT NULL,
  PRIMARY KEY (id)
)
ENGINE = INNODB;

--
-- Create index `test` on table `categoriastest`
--
ALTER TABLE categoriastest
ADD UNIQUE INDEX test (test, categoria);

--
-- Create foreign key
--
ALTER TABLE categoriastest
ADD CONSTRAINT fkcategoriasCategorias FOREIGN KEY (categoria)
REFERENCES categorias (id) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Create foreign key
--
ALTER TABLE categoriastest
ADD CONSTRAINT fkcategoriasTest FOREIGN KEY (test)
REFERENCES test (id) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Create table `categoriaspreguntas`
--
CREATE TABLE IF NOT EXISTS categoriaspreguntas (
  id int(11) NOT NULL AUTO_INCREMENT,
  categoria int(11) DEFAULT NULL,
  pregunta int(11) DEFAULT NULL,
  PRIMARY KEY (id)
)
ENGINE = INNODB;

--
-- Create index `pregunta` on table `categoriaspreguntas`
--
ALTER TABLE categoriaspreguntas
ADD UNIQUE INDEX pregunta (pregunta, categoria);

--
-- Create foreign key
--
ALTER TABLE categoriaspreguntas
ADD CONSTRAINT fkcategoriasCategorias1 FOREIGN KEY (categoria)
REFERENCES categorias (id) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Create foreign key
--
ALTER TABLE categoriaspreguntas
ADD CONSTRAINT fkcategoriasPregunta FOREIGN KEY (pregunta)
REFERENCES pregunta (id) ON DELETE CASCADE ON UPDATE CASCADE;

-- 
-- Restore previous SQL mode
-- 
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;

-- 
-- Enable foreign keys
-- 
/*!40014 SET FOREIGN_KEY_CHECKS = @OLD_FOREIGN_KEY_CHECKS */;